import datetime
import json

from django.test import Client as TestClient
from django.test import RequestFactory, TestCase
from gas_station.tasks import get_gas_stations
from gas_station.models import *


HOST = 'http://127.0.0.1:8090'
client = TestClient()

class ApiTestCase(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        GasStation.objects.create(id=101, price_92=100, price_95=120, price_98=200)
        GasStation.objects.create(id=102, price_92=80, price_95=110, price_98=120)
        Driver.objects.create(id=111,name='Петров', driver_experience=18, last_gas_station=GasStation.objects.get(id=1))
        Car.objects.create(mark='Audi', name='A8', type='Седан', owner=Driver.objects.get(id=1), avg_cons_100_km=14)

    def test_nearest_station(self):
        result = get_gas_stations(driver_id=111, hour_limit=2, stations_distances={'101': 10, '102': 30}, required_amount=100)
        self.assertEqual(json.loads(result)['nearest'], 101)

