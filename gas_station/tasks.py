from .models import *
import json
from django.db.models import Min


def get_gas_stations(driver_id: int, hour_limit: float, stations_distances: dict, required_amount: float,
                     speed: float = 30):
    '''

    :param driver_id: ID пользователя
    :param hour_limit: ограничение по времени
    :param stations_distances: словарь ID заправок и расстояния от пользователя
    :param required_amount: требуемый объем для заправки
    :param speed: скорость автомобиля до точки
    :return: json список удовлетворяющих требованиям заправок с оценкой затрат бензина на дорогу,
             времени и стоимости заправки
    '''

    stations = GasStation.objects.filter(id__in=list(stations_distances.keys()))
    car = Car.objects.get(owner_id=driver_id)
    required_max_distance = speed * hour_limit / 2
    good_distances = [distance for distance in list(stations_distances.values()) if distance < required_max_distance]
    # получение списка расстояний меньше максимального по ограничению по времени
    good_stations = []
    full_info = {}
    for good_distance in good_distances:
        station_id = list(stations_distances.keys())[list(stations_distances.values()).index(good_distance)]
        good_stations.append(station_id)
        # сформирован список подходящих заправок
        full_info[
            station_id] = {
            'consumption_on_route': round(float((car.avg_cons_100_km * good_distance) * 2),2),
            'Total_time': float(good_distance / speed) * 2,
            '92_gas': float(stations.get(id=station_id).price_92),

            '95_gas': float(stations.get(id=station_id).price_95),

            '98_gas': float(stations.get(id=station_id).price_98),
        }
        # формируется словарь с полной информацией о подходящих заправках
    try:
        # попытка получить минимальные по стоимости и ближайшую заправки
        minimum_price_92_station = float(stations.filter(
            id__in=good_stations).values_list('price_92').annotate(Min('price_92')).order_by('price_92')[0][0])
        minimum_price_95_station = float(stations.filter(
            id__in=good_stations).values_list('price_95').annotate(Min('price_95')).order_by('price_95')[0][0])
        minimum_price_98_station = float(stations.filter(
            id__in=good_stations).values_list('price_98').annotate(Min('price_98')).order_by('price_98')[0][0])
        nearest = list(stations_distances.keys())[list(stations_distances.values()).index(min(good_distances))]
    except:
        # обработка ошибок при отстутствии подходящих заправок
        minimum_price_92_station = None
        minimum_price_95_station = None
        minimum_price_98_station = None
        nearest = None
    # формирования json ответа со списком заправок
    return json.dumps({'result': {'full_info': full_info,
                                  'minimum_price_92_station': minimum_price_92_station,
                                  'minimum_price_95_station': minimum_price_95_station,
                                  'minimum_price_98_station': minimum_price_98_station,
                                  'nearest': nearest}
                       } if len(full_info) > 0 else {'result': 'Заправки не найдены'})
