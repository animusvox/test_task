from django.db import models





class GasStation(models.Model):
    '''
    Сущность заправочной станции
    '''
    price_92 = models.DecimalField(verbose_name='Цена на 92 бензин', decimal_places=2, max_digits=5)
    price_95 = models.DecimalField(verbose_name='Цена на 95 бензин', decimal_places=2, max_digits=5)
    price_98 = models.DecimalField(verbose_name='Цена на 98 бензин', decimal_places=2, max_digits=5)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Заправочная станция'
        verbose_name_plural = 'Заправочные станции'


class Driver(models.Model):
    '''
    Сущность пользователя
    '''
    name = models.CharField(verbose_name='ФИО водителя', max_length=255, )
    driver_experience = models.IntegerField(verbose_name='Стаж водителя')
    last_gas_station = models.ForeignKey(GasStation, verbose_name='Последняя посещенная заправка',
                                             on_delete=models.CASCADE,
                                             null=True, blank=True)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Car(models.Model):
    '''
    Сущность автомобиля
    '''
    types = (
        ('Седан', 'Седан'),
        ('Хэтчбек', 'Хэтчбек'),
        ('Универсал', 'Универсал'),
        ('Внедорожник', 'Внедорожник'),
        ('Минивен', 'Минивен'),
        ('Пикап', 'Пикап'),
        ('Кроссовер', 'Кроссовер'),
        ('Фургон', 'Фургон'),
    )
    mark = models.CharField(verbose_name='Название марки', max_length=255)
    name = models.CharField(verbose_name='Название модели', max_length=255)
    type = models.CharField(verbose_name='Тип автомобиля', choices=types, max_length=100)
    owner = models.ForeignKey(Driver, verbose_name='Владелец', on_delete=models.CASCADE)

    avg_cons_100_km = models.DecimalField(verbose_name='Средний расход на 100 км', decimal_places=2, max_digits=5)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'Автомобиль'
        verbose_name_plural = 'Автомобили'
